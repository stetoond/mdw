package machine;

/**
 * Predefined states of the ticket machine.
 * 
 * @author Ondrej Stetovsky
 *
 */
public enum State {
	NEW, PAYMENT, COMPLETED
}
