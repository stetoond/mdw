package storage;

import java.security.MessageDigest;
import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

/**
 * Stores the session id and some other data structure given as a generic
 * parameter.
 * 
 * @author Ondrej Stetovsky
 * @param <E>
 *            The generic parameter
 * 
 */
public class Sessions<E> {

	/**
	 * Storage for the session data
	 */
	private Hashtable<String, E> sessions = new Hashtable<String, E>();

	/**
	 * Returns session id based on the information in the http request
	 **/
	public String getSessionID(HttpServletRequest request) throws Exception {
		String sid = null;
		// extract the session id from the cookie
		if (request.getHeader("cookie") != null) {
			Pattern p = Pattern.compile(".*session-id=([a-zA-Z0-9]+).*");
			Matcher m = p.matcher(request.getHeader("cookie"));
			if (m.matches())
				sid = m.group(1);
		}
		// create the session id md5 hash; use random number to generate a
		// client-id
		// note that this is a simple solution but not very reliable
		if (sid == null || sessions.get(sid) == null) {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(new String(request.getRemoteAddr()
					+ Math.floor(Math.random() * 1000)).getBytes());

			sid = Utils.toHexString(md.digest());
		}
		return sid;
	}

	/**
	 * Returns session data from sessions object
	 * 
	 * @param sid
	 *            the session ID
	 * @return
	 */
	public E getData(String sid) {
		return sessions.get(sid);
	}

	/**
	 * Removes the session object with the specific session ID.
	 * 
	 * @param sid
	 *            the session ID
	 */
	public void removeData(String sid) {
		sessions.remove(sid);
	}

	/**
	 * Clears all data from all sessions.
	 */
	public void clearAllData() {
		sessions.clear();
	}

	/**
	 * Sets session data to sessions object
	 * 
	 * @param sid
	 *            the session ID
	 * @param d
	 *            the generic data
	 */
	public void setData(String sid, E d) {
		sessions.put(sid, d);
	}
}
