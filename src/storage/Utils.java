package storage;

/**
 * Utility to convert data types.
 * 
 * @author Ondrej Stetovsky
 * 
 */
public class Utils {

	public static String toHexString(byte[] digest) {
		StringBuilder sb = new StringBuilder();
		for (byte b : digest) {
			sb.append(String.format("%02x", b & 0xff));
		}

		return sb.toString();
	}

}
