package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import machine.State;
import storage.Sessions;

/**
 * The front-end server for client requirements.
 * 
 * @see HttpServlet
 * 
 * @author Ondrej Stetovsky
 */
public class RequirementServlet extends HttpServlet {
	
	/**
	 * Storage to store states of 
	 */
	private Sessions<State> sessions = new Sessions<State>();
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RequirementServlet() {
        super();
    }
    
    private void handleNoContentRequest(HttpServletRequest request, HttpServletResponse response) {
    	response.setStatus(204);
    }
	
	private void handleRequest(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("Request: " + request.toString());
		// get the session id
		String sid = null;
		try {
			sid = sessions.getSessionID(request);
		} catch (Exception e) {
			System.out.println("An exception occures durring getting the session ID from request.");
			e.printStackTrace();
		}
		// create the new data if none exists
		if (sessions.getData(sid) != null) {
			switch(sessions.getData(sid)) {
			case NEW:
				sessions.setData(sid, State.PAYMENT);
				break;
			case PAYMENT:
				sessions.setData(sid, State.COMPLETED);
				break;
			case COMPLETED:
				// this two steps can be split into one step
				sessions.removeData(sid); // delete an old client and create a new one
				sessions.setData(sid, State.NEW);
				break;
			}
			
		} else {
			sessions.setData(sid, State.NEW);
		}
		
		// send the response
		response.setStatus(200);
		response.setHeader("Set-Cookie", "session-id="+ sid + "; MaxAge=3600");
		response.setHeader("Content-Type", "text/plain");
		try {
			response.getWriter().write("Ticket machine is in state: " +
			sessions.getData(sid).toString());
			response.flushBuffer();
		} catch (IOException e) {
			System.out.println("An exception occures durring sending response to a client.");
			e.printStackTrace();
		}
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		sessions.clearAllData();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		handleRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		handleRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		handleNoContentRequest(request, response);
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		handleNoContentRequest(request, response);
	}

	/**
	 * @see HttpServlet#doOptions(HttpServletRequest, HttpServletResponse)
	 */
	protected void doOptions(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		handleNoContentRequest(request, response);
	}

	/**
	 * @see HttpServlet#doTrace(HttpServletRequest, HttpServletResponse)
	 */
	protected void doHead(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		handleNoContentRequest(request, response);
	}

}
